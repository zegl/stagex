FROM scratch as base
ENV VERSION=0.0.5
ENV SRC_FILE=v${VERSION}.tar.gz
ENV SRC_SITE=https://codeberg.org/stagex/sxctl/archive/${SRC_FILE}
ENV SRC_HASH=26f0dd95e72b632036c598d055ef6b78cec88c497dd67ede30d1498bb8229412

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/busybox . /
COPY --from=stagex/go . /
RUN tar -xf ${SRC_FILE}
WORKDIR sxctl/cmd/sxctl
RUN go build -o sxctl

FROM build as install
WORKDIR /rootfs
RUN cp /sxctl/cmd/sxctl/sxctl .
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM stagex/filesystem as package
COPY --from=install /rootfs/. /
ENTRYPOINT ["/sxctl"]
