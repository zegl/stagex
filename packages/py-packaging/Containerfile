FROM scratch as base
ENV VERSION=24.0
ENV SRC_HASH=eb82c5e3e56209074766e6885bb04b8c38a0c015d0a30036ebe7ece34c9989e9
ENV SRC_FILE=packaging-${VERSION}.tar.gz
ENV SRC_SITE=https://files.pythonhosted.org/packages/source/p/packaging/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as install
COPY --from=stagex/busybox . /
COPY --from=stagex/musl . /
COPY --from=stagex/python . /
COPY --from=stagex/py-flit . /
COPY --from=stagex/py-gpep517 . /
COPY --from=stagex/zlib . /
RUN tar -xzf ${SRC_FILE}
WORKDIR packaging-${VERSION}/src
RUN <<-EOF
	set -eu
	sitedir="$(python3 -c 'import site;print(site.getsitepackages()[0])')"
	mkdir -p "/rootfs/${sitedir}"
	cp -a packaging "/rootfs/$sitedir"
    find /rootfs | grep -E "(/__pycache__$|\.pyc$|\.pyo$)" | xargs rm -rf
	find /rootfs -exec touch -hcd "@0" "{}" +
EOF

FROM stagex/filesystem as package
COPY --from=install /rootfs/. /
