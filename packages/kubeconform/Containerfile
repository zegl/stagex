FROM scratch as base
ENV VERSION=0.6.4
ENV SRC_HASH=fa5f1f7de0d6cd97106b70965c6275cc5e7afb22ff6e2459a94f8f33341b5c93
ENV SRC_FILE=v${VERSION}.tar.gz
ENV SRC_SITE=https://github.com/yannh/kubeconform/archive/refs/tags/${SRC_FILE}
ENV GOPATH=/cache/go
ENV GOCACHE=/cache/
ENV GOWORK=off
ENV GOPROXY=https://proxy.golang.org,direct
ENV GOSUMDB=sum.golang.org
ENV CGO_ENABLED=0
ENV GOHOSTOS=linux
ENV GOHOSTARCH=amd64
ENV GO11MODULE=off

FROM base as fetch
COPY --from=stagex/busybox . /
COPY --from=stagex/go . /
COPY --from=stagex/ca-certificates . /
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .
RUN tar -xvf ${SRC_FILE}
WORKDIR kubeconform-${VERSION}
RUN go get ./...

FROM fetch as build
RUN --network=none go build -o bin/ -trimpath -v ./...

from build as install
RUN <<-EOF
    set -eu
    mkdir -p /rootfs/usr/bin/
    cp bin/kubeconform /rootfs/usr/bin/
    find /rootfs -exec touch -hcd "@0" "{}" +
EOF

FROM stagex/filesystem as package
COPY --from=install /rootfs/./ /
