FROM scratch as base
ENV VERSION=2.15
ENV SRC_HASH=efa50ef983137eefc0a02fdb51509d624b5e3295c980aa127ceee4183455499e
ENV SRC_FILE=cpio-${VERSION}.tar.gz
ENV SRC_SITE=https://ftp.gnu.org/gnu/cpio/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stagex/busybox . /
COPY --from=stagex/gcc . /
COPY --from=stagex/binutils . /
COPY --from=stagex/make . /
COPY --from=stagex/musl . /
RUN tar -xzf ${SRC_FILE}
WORKDIR cpio-${VERSION}
RUN --network=none <<-EOF
    set -eux
    ./configure \
        --build=x86_64-unknown-linux-musl \
        --host=x86_64-unknown-linux-musl \
        --prefix=/usr \
        --bindir=/usr/bin \
        --mandir=/usr/share/man \
        --infodir=/usr/share/info \
        gl_cv_func_getcwd_path_max="no, but it is partly working"
    make -j "$(nproc)"
EOF

FROM build as install
RUN <<-EOF
    set -eux
    make DESTDIR=/rootfs install
EOF
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM stagex/filesystem as package
COPY --from=install /rootfs/. /
