FROM scratch as base
ENV VERSION=4.42.1
ENV SRC_HASH=be31e5e828a0251721ea71964596832d4a40cbc21c8a8392a804bc8d1c55dd62
ENV SRC_FILE=v${VERSION}.tar.gz
ENV SRC_SITE=https://github.com/mikefarah/yq/archive/refs/tags/${SRC_FILE}
ENV GOPATH=/cache/go
ENV GOCACHE=/cache/
ENV GOWORK=off
ENV GOPROXY=https://proxy.golang.org,direct
ENV GOSUMDB=sum.golang.org
ENV CGO_ENABLED=0
ENV GOHOSTOS=linux
ENV GOHOSTARCH=amd64
ENV GO11MODULE=on

FROM base as fetch
COPY --from=stagex/busybox . /
COPY --from=stagex/go . /
COPY --from=stagex/ca-certificates . /
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .
RUN tar -xvf ${SRC_FILE}
WORKDIR yq-${VERSION}
RUN go get ./...

FROM fetch as build
RUN --network=none go build -v -trimpath

from build as install
RUN <<-EOF
    mkdir -p /rootfs/usr/bin/
    cp yq /rootfs/usr/bin/
    find /rootfs -exec touch -hcd "@0" "{}" +
EOF

FROM stagex/filesystem as package
COPY --from=install /rootfs/./ /
