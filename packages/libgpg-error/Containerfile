FROM scratch as base
ENV VERSION=1.47
ENV SRC_HASH=9e3c670966b96ecc746c28c2c419541e3bcb787d1a73930f5e5f5e1bcbbb9bdb
ENV SRC_FILE=libgpg-error-${VERSION}.tar.bz2
ENV SRC_SITE=https://gnupg.org/ftp/gcrypt/libgpg-error/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} ${SRC_FILE}

FROM fetch as build
COPY --from=stagex/busybox . /
COPY --from=stagex/musl . /
COPY --from=stagex/gcc . /
COPY --from=stagex/binutils . /
COPY --from=stagex/make . /
COPY --from=stagex/npth . /
RUN tar -xvf $SRC_FILE
WORKDIR libgpg-error-${VERSION}
ENV SOURCE_DATE_EPOCH=1
RUN --network=none <<-EOF
	set -eux
	./configure \
		--build=x86_64-unknown-linux-musl \
		--host=x86_64-unknown-linux-musl \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make -j "$(nproc)"
EOF

FROM build as install
RUN make DESTDIR=/rootfs install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM base as test
COPY --from=install /rootfs/. /
COPY --from=stagex/busybox . /
RUN /bin/sh <<-EOF
	set -eux
	LIBGPG_ERROR_FILES_FOUND=\$(ls /usr/lib/ | grep libgpg-error || true)
	if [ -z "\$LIBGPG_ERROR_FILES_FOUND" ]; then
	    echo "libgpg-error not found"
	    exit 1
	fi
EOF

FROM stagex/filesystem as package
COPY --from=install /rootfs/. /
